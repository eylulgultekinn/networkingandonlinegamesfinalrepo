﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public float moveSpeed = 3.5f;
    public float TurnSpeed = 120f;
    private TextMesh tm = null;
    private Text ui = null;

    private void Start()
    {
        SetTitle("Game Title");
        SetPlayerCaptiion("1");
    }

    private void Update()
    {
        float vert = Input.GetAxis("Vertical");
        float horz = Input.GetAxis("Horizontal");
        this.transform.Translate(Vector3.forward * vert * moveSpeed * Time.deltaTime);
        this.transform.localRotation *= Quaternion.AngleAxis(horz * TurnSpeed * Time.deltaTime, Vector3.up);
    }

    public void SetPlayerCaptiion(string caption)
    {
        if(tm == null)
        {
            for(int i =0; i< this.transform.childCount; i++)
            {
                if(this.transform.GetChild(i).name == "Caption")
                {
                    tm = this.transform.GetChild(i).GetComponent<TextMesh>();
                }
            }
        }

        if(tm != null)
        {
            tm.text = caption;
        }
        else
        {
            tm.text = "err";
        }
    }

    public void SetTitle(string caption)
    {
        if(ui == null)
        {
            ui = GameObject.Find("textTitle").GetComponent<Text>();
        }

        if(ui != null)
        {
            ui.text = caption;
        }
    }

}
