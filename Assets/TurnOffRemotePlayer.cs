﻿using UnityEngine.Networking;
using UnityEngine;
using Mirror;

public class TurnOffRemotePlayer : NetworkBehaviour
{

    private void Start()
    {
        string id = string.Format("{0}", this.netId);
        Player scr = this.GetComponent<Player>();

        if(this.isLocalPlayer== true)
        {
            scr.enabled = true;
            scr.SetPlayerCaptiion(id);
            scr.SetTitle("Multi Player #" + id);
        }
        else
        {
            scr.SetPlayerCaptiion(id);
            scr.enabled = false;
        }
    }


}
